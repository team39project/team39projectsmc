#pragma once
#include "State.h"
#include "GameWinningState.h"
#include "../../Clases/GameLogic/Header/YRules.h"
#include "Player.h"
#include "../../Clases/GameLogic/Header/HavannahRules.h"
#include "../../Clases/GameLogic/Header/HexRules.h"
#include <SFML/Audio.hpp>

class GamesWindow
	:public State
{
public:
	GamesWindow(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~GamesWindow()override;
	void configureWindowDesign(sf::RectangleShape& rectangle);
	void configurePieRule(sf::RectangleShape& rectButton, sf::Text& boardText, sf::Font& boardFont);
	void configureQuitSign(sf::RectangleShape& rectButton, sf::Text& boardText, sf::Font& boardFont);
	void activatePieRuleButton(sf::RectangleShape& rectButton, sf::Text& boardText);
	void deactivatePieRuleButton(sf::RectangleShape& rectButton, sf::Text& boardText);
	void showBoard(const int& choice);
	void winningSituation(const bool& ok, sf::Color color);
	void isKeyPressed();
	void endState()override;
	void updateButtons();
	void update()override;
	void renderButtons(sf::RenderTarget* target = nullptr);
	void render(sf::RenderTarget* target = nullptr)override;
private:
	int xCoord, yCoord, shapeSide, maxCol, maxDepth, yProiection, chosenGameIndice;
	int chosenGamesIndice;
	bool presentProiection = true, goodChoice = true;
	sf::Texture background_texture;
	sf::RectangleShape background;
	sf::Font font;
	std::map<std::string, Button*>buttons;
	void initImage();
	void initFont();
	void initButton();

};

