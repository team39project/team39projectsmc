#pragma once
#include "State.h"
#include "GameMenuState.h"

class Game
{
public:
	Game();
	virtual ~Game();
	void endApp();
	void updateEvent();
	void update();
	void render();
	void run();
private:
	sf::RenderWindow* window;
	sf::Event event;
	void initWindow();
	void initState();
	std::stack<State*>state;
};