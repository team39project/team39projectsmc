#pragma once
#include <stack>
#include <vector>
#include "Board.h"

class State
{
public:
	State(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~State();
	const bool& getEnd() const;
	virtual void isEndPressed();
	virtual void endState() = 0;
	virtual void updatemousePosition();
	virtual void update();
	virtual void render(sf::RenderTarget* target = NULL);

protected:
	std::stack<State*>* states;
	sf::RenderWindow* window;
	std::vector<sf::Texture>texture;
	bool end;
	sf::Vector2i mousePosScreen;
	sf::Vector2i mousePosWindow;
	sf::Vector2f mousePosView;

};

