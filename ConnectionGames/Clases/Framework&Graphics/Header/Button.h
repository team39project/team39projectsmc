#pragma once
#include "State.h"

enum button_states {
	cur_btn = 0,
	btn_hover,
	btn_pressed
};
class Button
{
public:
	Button(float x, float y, float width, float height, sf::Font* font, std::string text, sf::Color initialColor, sf::Color hoverColor, sf::Color currentColor);
	~Button();
	const bool getButtonPressed() const;
	void update(const sf::Vector2f mousePosition);
	void render(sf::RenderTarget* target);

private:

	sf::RectangleShape shape;
	sf::Color initialColor;
	sf::Color hoverColor;
	sf::Color currentColor;
	sf::Font* font;
	sf::Text text;
	bool pressed;
	short unsigned button_state;

};

