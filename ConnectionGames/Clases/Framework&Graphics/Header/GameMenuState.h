#pragma once
#include "State.h"
#include <stack>
#include "Button.h"
#include "GamesWindow.h"


class GameMenuState
	:public State
{
public:
	GameMenuState(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~GameMenuState()override;
	void endState() override;
	void updateButtons();
	void update() override;
	void renderButtons(sf::RenderTarget* target = nullptr);
	void render(sf::RenderTarget* target = nullptr) override;
private:
	sf::Texture background_texture;
	sf::RectangleShape background;
	sf::Font font;
	std::map<std::string, Button*>buttons;
	void initImage();
	void initFont();
	void initButton();
};

