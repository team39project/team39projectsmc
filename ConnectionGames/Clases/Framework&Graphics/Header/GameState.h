#pragma once
#include "State.h"
class GameState
	:public State
{
public:
	GameState(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~GameState()override;
	void isKeyPressed();
	void endState()override;
	void update()override;
	void render(sf::RenderTarget* target = nullptr)override;
private:
	Board newGame;
};

