#pragma once
#include "Board.h"
#include<vector>

class Player
{
public:
	Player(sf::Color color);
	sf::Color getPlayerColor();
private:
	sf::Color color;
	std::vector<std::pair<int, int>> PositionsOfPlayer;
};
