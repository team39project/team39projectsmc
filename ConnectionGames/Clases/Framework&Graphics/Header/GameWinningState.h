#pragma once
#include "State.h"
#include "Button.h"
#include "GamesWindow.h"
#include "GameMenuState.h"
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

class GameWinningState
	:public State
{
public:
	GameWinningState(sf::RenderWindow* window, std::stack<State*>* states, sf::Color color);
	virtual ~GameWinningState()override;
	void endState()override;
	void updateButtons();
	void update()override;
	void isEndPressed() override;
	void renderButtons(sf::RenderTarget* target = nullptr);
	void render(sf::RenderTarget* target = nullptr)override;
private:
	sf::Texture background_texture;
	sf::RectangleShape background;
	sf::Font font;
	sf::Text text;
	sf::SoundBuffer buffer;
	sf::Sound sound;
	std::map<std::string, Button*>buttons;

	void initImage();
	void initFont();
	void initButton();
	void initText();
	void initBuffer();
	void initSound();
	void initWindow(sf::Color color);
};