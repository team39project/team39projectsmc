#include "../Header/GameMenuState.h"
GameMenuState::GameMenuState(sf::RenderWindow* window, std::stack<State*>* states)
	: State(window, states)
{
	this->initImage();
	this->initFont();
	this->initButton();
}

GameMenuState::~GameMenuState()
{
	for (auto index = buttons.begin(); index != buttons.end(); ++index)
	{
		delete index->second;
	}
}

void GameMenuState::endState()
{
}

void GameMenuState::updateButtons()
{
	for (auto& index : this->buttons)
	{
		index.second->update(this->mousePosView);
	}

	if (this->buttons["Start_Game"]->getButtonPressed())
	{
		this->states->push(new GamesWindow(this->window, this->states));
	}

	if (this->buttons["Exit_Game"]->getButtonPressed())
	{
		this->end = true;
	}
}

void GameMenuState::update()
{
	this->updatemousePosition();
	this->updateButtons();
}

void GameMenuState::renderButtons(sf::RenderTarget* target)
{
	for (auto& index : this->buttons)
	{
		index.second->render(target);
	}
}

void GameMenuState::render(sf::RenderTarget* target)
{
	if (!target)
	{
		target = this->window;
	}
	target->draw(this->background);
	this->renderButtons(target);
}

void GameMenuState::initImage()
{
	this->background.setSize(sf::Vector2f(static_cast<float>(this->window->getSize().x), static_cast<float>(this->window->getSize().y)));

	if (!this->background_texture.loadFromFile("Background/backgroundSMC.jpeg"))
	{
		throw("Could not load image from file");
	}

	this->background.setTexture(&this->background_texture);
}

void GameMenuState::initFont()
{
	if (!this->font.loadFromFile("arial.ttf"))
	{
		throw("Could not load font from file");
	}
}

void GameMenuState::initButton()
{
	this->buttons["Connection Games"] = new Button(200, 50, 150, 100, &this->font, "Connection Games", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
	this->buttons["Start_Game"] = new Button(200, 250, 150, 50, &this->font, "START", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
	this->buttons["Exit_Game"] = new Button(200, 350, 150, 50, &this->font, "EXIT", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
}
