#include "../Header/GamesWindow.h"


GamesWindow::GamesWindow(sf::RenderWindow* window, std::stack<State*>* states)
	:State(window, states)
{
	this->initImage();
	this->initFont();
	this->initButton();
}

GamesWindow::~GamesWindow()
{
	for (auto index = buttons.begin(); index != buttons.end(); ++index)
	{
		delete index->second;
	}
}

void GamesWindow::configureWindowDesign(sf::RectangleShape& rectangle)
{
	rectangle.setPosition(248, 0);
	rectangle.setOutlineThickness(5);
	rectangle.setOutlineColor(sf::Color::Black);
	sf::Color backColor(105, 105, 105);
	rectangle.setFillColor(backColor);
}

void GamesWindow::configurePieRule(sf::RectangleShape& rectButton, sf::Text& boardText, sf::Font& boardFont)
{
	const int pieRuleX = 300;
	const int pieRuleY = 60;
	rectButton.setPosition(pieRuleX, pieRuleY);
	rectButton.setFillColor(sf::Color::Transparent);
	rectButton.setOutlineThickness(4);
	rectButton.setOutlineColor(sf::Color::Transparent);
	boardText.setFont(boardFont);
	boardText.setString("Pie rule?");
	boardText.setCharacterSize(25);
	const int textX = 340, textY = 75;
	boardText.setPosition(textX, textY);
	boardText.setOutlineThickness(2);
	boardText.setOutlineColor(sf::Color::Transparent);
	boardText.setFillColor(sf::Color::Transparent);
}

void GamesWindow::configureQuitSign(sf::RectangleShape& rectButton, sf::Text& boardText, sf::Font& boardFont)
{
	const int quitSignX = 20;
	const int quitSignY = 505;
	sf::Color newColor(255, 71, 26);
	rectButton.setPosition(quitSignX, quitSignY);
	rectButton.setFillColor(newColor);
	rectButton.setOutlineThickness(4);
	rectButton.setOutlineColor(sf::Color::Black);
	boardText.setFont(boardFont);
	boardText.setString("Press ENTER to quit");
	boardText.setCharacterSize(20);
	const int textX = 25, textY = 522;
	boardText.setPosition(textX, textY);
	boardText.setOutlineThickness(2);
	boardText.setOutlineColor(sf::Color::Black);
	boardText.setFillColor(sf::Color::White);
}

void GamesWindow::activatePieRuleButton(sf::RectangleShape& rectButton, sf::Text& boardText)
{
	sf::Color blueOutline(0, 0, 205);
	rectButton.setFillColor(sf::Color::Yellow);
	rectButton.setOutlineColor(blueOutline);
	boardText.setOutlineColor(blueOutline);
	boardText.setFillColor(sf::Color::White);
}

void GamesWindow::deactivatePieRuleButton(sf::RectangleShape& rectButton, sf::Text& boardText)
{
	rectButton.setFillColor(sf::Color::Transparent);
	rectButton.setOutlineColor(sf::Color::Transparent);
	boardText.setOutlineColor(sf::Color::Transparent);
	boardText.setFillColor(sf::Color::Transparent);
}

void GamesWindow::showBoard(const int& choice)
{
	bool ok = false;
	Board newGame;
	sf::RenderWindow window(sf::VideoMode(300, 300, 32), "Connection Games", sf::Style::Fullscreen);
	newGame.drawBoard(xCoord, yCoord, yProiection, shapeSide, maxCol, maxDepth, presentProiection);
	Player firstPlayer(sf::Color::White);
	Player secondPlayer(sf::Color::Black);
	std::queue<Player> turn;
	turn.push(firstPlayer);
	turn.push(secondPlayer);
	int moveNumber = 0;
	sf::Texture texture;
	if (!texture.loadFromFile("boardBackground.jpg"))
	{
		std::cout << "Photo not found!";
	}
	sf::Sprite windowSprite;
	windowSprite.setTexture(texture);
	sf::RectangleShape rectangle(sf::Vector2f(1477, 1100));
	configureWindowDesign(rectangle);
	sf::RectangleShape rectButton(sf::Vector2f(180, 60));
	sf::Font boardFont;
	if (!boardFont.loadFromFile("arial.TTF"))
		std::cout << "Font file not found!";
	sf::Text boardText, boardTextQuit;
	sf::SoundBuffer buffer;
	if (!buffer.loadFromFile("button sound.wav"))
		throw("No audio file!");
	sf::Sound sound;
	sound.setBuffer(buffer);
	sf::RectangleShape rectSign(sf::Vector2f(200, 60));
	configureQuitSign(rectSign, boardTextQuit, boardFont);
	configurePieRule(rectButton, boardText, boardFont);
	while (window.isOpen())
	{
		window.draw(windowSprite);
		window.draw(rectangle);
		window.draw(rectSign);
		window.draw(boardTextQuit);
		if (moveNumber == 1)
			activatePieRuleButton(rectButton, boardText);
		window.draw(rectButton);
		window.draw(boardText);
		newGame.repaint(window);
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::MouseButtonPressed:
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					int xCoord = sf::Mouse::getPosition().x;
					int yCoord = sf::Mouse::getPosition().y;
					Player currentTurn = turn.front();
					if (moveNumber == 1 && newGame.clickedPieRule(xCoord, yCoord, rectButton))
					{
						newGame.pieRule();
						turn.pop();
						turn.push(currentTurn);
						++moveNumber;
						deactivatePieRuleButton(rectButton, boardText);
						break;
					}
					int matrixLine = -1, matrixColumn = -1;
					if (newGame.clickHexagon(xCoord, yCoord, currentTurn.getPlayerColor(), matrixLine, matrixColumn) == true)
					{
						sound.play();
						std::vector<std::pair<int, int>> edgesIndices;
						std::vector<bool> foundEdges;
						std::vector<std::vector<std::tuple<int, int, sf::CircleShape, int> > > coordinatesMatrix = newGame.getMatrix();
						sf::Color playerColor = currentTurn.getPlayerColor();
						if (choice == 1)
						{
							const int havannahCornersNumber = 6;
							std::vector<bool> foundCorners;
							std::array<std::pair<int, int>, havannahCornersNumber> havannahCorners;
							HavannahRules obj(edgesIndices, foundEdges, havannahCorners, foundCorners);
							std::pair<int, int> newPair = std::pair<int, int>(matrixLine, matrixColumn);
							if ((obj.havannahBfs(foundEdges, matrixLine, matrixColumn, coordinatesMatrix, edgesIndices, havannahCorners, foundCorners) == true))
							{
								ok = true;
								window.close();
								sf::Color currentColor = currentTurn.getPlayerColor();
								winningSituation(ok, currentColor);
							}
							else
							{
								std::vector<std::vector<std::tuple<int, int, sf::CircleShape, int>> > dfsMatrix;
								dfsMatrix = newGame.getMatrix();
								const int mark = 2;
								std::get<3>(dfsMatrix[matrixLine][matrixColumn]) = 1;
								if (obj.havannahCircleWinningCondition(matrixLine, matrixColumn, dfsMatrix, playerColor, mark) == true)
								{
									ok = true;
									window.close();
									sf::Color currentColor = currentTurn.getPlayerColor();
									winningSituation(ok, currentColor);
								}
							}
						}
						else
						{
							if (choice == 2)
							{
								HexRules hexObject(edgesIndices, foundEdges);
								if (hexObject.hexBfs(foundEdges, matrixLine, matrixColumn, coordinatesMatrix, edgesIndices) == true)
								{
									ok = true;
									window.close();
									sf::Color currentColor = currentTurn.getPlayerColor();
									winningSituation(ok, currentColor);
								}
							}
							else
							{
								YRules yObject(edgesIndices, foundEdges);
								if (yObject.yBfs(foundEdges, matrixLine, matrixColumn, coordinatesMatrix, edgesIndices) == true)

								{
									ok = true;
									window.close();
									sf::Color currentColor = currentTurn.getPlayerColor();
									winningSituation(ok, currentColor);
								}
							}
						}
						turn.pop();
						turn.push(currentTurn);
						++moveNumber;
					}
					if (moveNumber == 2)
						deactivatePieRuleButton(rectButton, boardText);
				}
				break;
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
		{
			window.close();
		}
	}
}

void GamesWindow::winningSituation(const bool& ok, sf::Color color)
{
	if (ok)
	{
		this->states->push(new GameWinningState(this->window, this->states, color));
	}
}


void GamesWindow::isKeyPressed()
{
	this->isEndPressed();
}

void GamesWindow::endState()
{
	std::cout << "Ending Game" << std::endl;
}

void GamesWindow::updateButtons()
{
	for (auto& index : this->buttons)
	{
		index.second->update(this->mousePosView);
	}

	if (this->buttons["Havannah"]->getButtonPressed())
	{
		xCoord = 735;
		yCoord = 180;
		shapeSide = 10;
		maxCol = 9;
		maxDepth = 18;
		yProiection = 864;
		presentProiection = true;
		chosenGamesIndice = 1;
		showBoard(chosenGamesIndice);
	}

	if (this->buttons["Hex"]->getButtonPressed())
	{
		xCoord = 928;
		yCoord = 135;
		shapeSide = 11;
		maxCol = 0;
		maxDepth = 20;
		yProiection = 895;
		presentProiection = true;
		chosenGamesIndice = 2;
		showBoard(chosenGamesIndice);
	}

	if (this->buttons["Y"]->getButtonPressed())
	{
		xCoord = 910;
		yCoord = 250;
		shapeSide = 13;
		maxCol = 0;
		maxDepth = 12;
		yProiection = -100;
		presentProiection = false;
		chosenGamesIndice = 3;
		showBoard(chosenGamesIndice);
	}
}

void GamesWindow::update()
{
	this->isKeyPressed();
	this->updatemousePosition();
	this->updateButtons();
}

void GamesWindow::renderButtons(sf::RenderTarget* target)
{
	for (auto& index : this->buttons)
	{
		index.second->render(target);
	}
}

void GamesWindow::render(sf::RenderTarget* target)
{
	if (!target)
	{
		target = this->window;
	}
	target->draw(this->background);
	this->renderButtons(target);
}

void GamesWindow::initImage()
{
	this->background.setSize(sf::Vector2f(static_cast<float>(this->window->getSize().x), static_cast<float>(this->window->getSize().y)));

	if (!this->background_texture.loadFromFile("Background/backgroundSMC.jpeg"))
	{
		throw("Could not load image from file");
	}

	this->background.setTexture(&this->background_texture);
}

void GamesWindow::initFont()
{
	if (!this->font.loadFromFile("arial.ttf"))
	{
		throw("Could not load font from file");
	}
}

void GamesWindow::initButton()
{
	this->buttons["Havannah"] = new Button(200, 100, 150, 50, &this->font, "Havannah", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
	this->buttons["Hex"] = new Button(200, 200, 150, 50, &this->font, "Hex", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
	this->buttons["Y"] = new Button(200, 300, 150, 50, &this->font, "Y", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
}
