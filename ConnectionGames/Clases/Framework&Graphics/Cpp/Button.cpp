#include "../Header/Button.h"

Button::Button(float x, float y, float width, float height, sf::Font* font, std::string text, sf::Color initialColor, sf::Color hoverColor, sf::Color currentColor)
{
	this->button_state = cur_btn;

	this->shape.setPosition(sf::Vector2f(x, y));
	this->shape.setSize(sf::Vector2f(width, height));


	this->font = font;
	this->text.setFont(*this->font);
	this->text.setString(text);
	this->text.setFillColor(sf::Color::Black);
	this->text.setCharacterSize(12);
	this->text.setPosition(this->shape.getPosition().x + (this->shape.getGlobalBounds().width / 2.f) - this->text.getGlobalBounds().width / 2.f, this->shape.getPosition().y + (this->shape.getGlobalBounds().height / 2.f) - this->text.getGlobalBounds().height / 2.f);


	this->initialColor = initialColor;
	this->hoverColor = hoverColor;
	this->currentColor = currentColor;

	this->shape.setFillColor(this->initialColor);

}

Button::~Button()
{
}

const bool Button::getButtonPressed() const
{
	if (this->button_state == btn_pressed)
	{
		return true;
	}
	return false;
}

void Button::update(const sf::Vector2f mousePosition)
{
	this->button_state = cur_btn;
	if (this->shape.getGlobalBounds().contains(mousePosition))
	{
		this->button_state = btn_hover;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			this->button_state = btn_pressed;
		}
	}
	switch (this->button_state)
	{
	case cur_btn:
		this->shape.setFillColor(this->initialColor);
		break;
	case btn_hover:
		this->shape.setFillColor(this->hoverColor);
		break;
	case btn_pressed:
		this->shape.setFillColor(this->currentColor);
		break;
	default:
		this->shape.setFillColor(sf::Color::Red);
		break;

	}
}

void Button::render(sf::RenderTarget* target)
{
	target->draw(this->shape);
	target->draw(this->text);
}
