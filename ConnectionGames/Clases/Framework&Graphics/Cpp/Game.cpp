#include "../Header/Game.h"
#include <SFML/Audio.hpp>

void Game::initWindow()
{
	this->window = new sf::RenderWindow(sf::VideoMode(600, 600), "Conection Games", sf::Style::Titlebar | sf::Style::Resize | sf::Style::Close);
	this->window->setFramerateLimit(120);
}

void Game::initState()
{
	this->state.push(new GameMenuState(this->window, &this->state));
}

Game::Game()
{
	this->initWindow();
	this->initState();
}

Game::~Game()
{
	delete this->window;
	while (!this->state.empty())
	{
		delete this->state.top();
		this->state.pop();
	}
}

void Game::endApp()
{
	std::cout << "Ending game" << std::endl;

}

void Game::updateEvent()
{
	while (this->window->pollEvent(this->event))
	{
		if (this->event.type == sf::Event::Closed)
		{
			this->window->close();
		}
	}
}

void Game::update()
{
	this->updateEvent();
	if (!this->state.empty())
	{
		this->state.top()->update();

		if (this->state.top()->getEnd())
		{
			this->state.top()->endState();
			delete this->state.top();
			this->state.pop();
		}
	}
	else
	{
		this->endApp();
		this->window->close();
	}
}

void Game::render()
{
	this->window->clear();

	if (!this->state.empty())
	{
		this->state.top()->render(this->window);
	}
	this->window->display();
}

void Game::run()
{
	while (this->window->isOpen())
	{
		this->update();
		this->render();
	}
}

