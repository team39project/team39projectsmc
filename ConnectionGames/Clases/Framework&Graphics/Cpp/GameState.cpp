#include "../Header/GameState.h"
GameState::GameState(sf::RenderWindow* window, std::stack<State*>* states)
	: State(window, states)
{
}

GameState::~GameState()
{
}

void GameState::isKeyPressed()
{
	this->isEndPressed();
}

void GameState::endState()
{
	std::cout << "Ending Game" << std::endl;
}


void GameState::update()
{
	this->isKeyPressed();
	this->updatemousePosition();

}

void GameState::render(sf::RenderTarget* target)
{
	if (!target)
	{
		target = this->window;
	}
}