#include "../Header/State.h"

State::State(sf::RenderWindow* window, std::stack<State*>* states)
{
	this->window = window;
	this->states = states;
	this->end = false;
}

State::~State()
{
}

const bool& State::getEnd() const
{
	// TODO: insert return statement here
	return this->end;
}

void State::isEndPressed()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
	{
		this->end = true;
	}
}

void State::updatemousePosition()
{
	this->mousePosScreen = sf::Mouse::getPosition();
	this->mousePosWindow = sf::Mouse::getPosition(*this->window);
	this->mousePosView = this->window->mapPixelToCoords(sf::Mouse::getPosition(*this->window));
}

void State::update()
{
}

void State::render(sf::RenderTarget* target)
{
}