#include "../Header/GameWinningState.h"

void GameWinningState::initImage()
{
	this->background.setSize(sf::Vector2f(static_cast<float>(this->window->getSize().x), static_cast<float>(this->window->getSize().y)));

	if (!this->background_texture.loadFromFile("Background/backgroundSMC.jpeg"))
	{
		throw("Could not load image from file");
	}

	this->background.setTexture(&this->background_texture);
}

void GameWinningState::initFont()
{
	if (!font.loadFromFile("Action_Force.ttf"))
	{
		throw("Unknown font.");
	}
}

void GameWinningState::initButton()
{
	this->buttons["Start_Game"] = new Button(200, 250, 150, 50, &this->font, "START AGAIN", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
	this->buttons["Exit_Game"] = new Button(200, 350, 150, 50, &this->font, "EXIT", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
}

void GameWinningState::initText()
{
	text.setFont(font);
	text.setCharacterSize(24);
	text.setFillColor(sf::Color::Red);
}

void GameWinningState::initBuffer()
{
	if (!buffer.loadFromFile("Ta Da.wav"))
		throw("Could not load the sound from file");
}

void GameWinningState::initSound()
{
	sound.setBuffer(buffer);
}

void GameWinningState::initWindow(sf::Color color)
{
	sf::RenderWindow window(sf::VideoMode(1000, 1000), " ");
	std::string colorString;
	if (color == sf::Color::Black)
		colorString = "Black";
	else
		colorString = "White";
	std::string stringText = "Congratulations! \n " + colorString + " won the game!!! ";

	text.setString(stringText);

	int isOpen = 0;
	while (window.isOpen())
	{
		if (isOpen == 0)
			sound.play();
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		isOpen = 1;
		text.move(0.01f, 0.f);
		if (text.getPosition().x == window.getPosition().x)
		{
			text.setPosition(1000, 1000);
		}
		window.clear();
		window.draw(text);
		text.setPosition(400, 400);
		window.display();
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
		{
			window.close();
		}
	}
}

GameWinningState::GameWinningState(sf::RenderWindow* window, std::stack<State*>* states, sf::Color color)
	: State(window, states)
{
	this->initImage();
	this->initFont();
	this->initButton();
	this->initText();
	this->initBuffer();
	this->initSound();
	this->initWindow(color);
}

GameWinningState::~GameWinningState()
{
	for (auto index = buttons.begin(); index != buttons.end(); ++index)
	{
		delete index->second;
	}
}

void GameWinningState::endState()
{
}

void GameWinningState::updateButtons()
{
	for (auto& index : this->buttons)
	{
		index.second->update(this->mousePosView);
	}

	if (this->buttons["Start_Game"]->getButtonPressed())
	{
		this->states->push(new GameMenuState(this->window, this->states));
	}

	if (this->buttons["Exit_Game"]->getButtonPressed())
	{
		this->end = true;
	}
}

void GameWinningState::update()
{
	this->updatemousePosition();
	this->updateButtons();
}

void GameWinningState::isEndPressed()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
	{
		this->end = true;
	}
}

void GameWinningState::renderButtons(sf::RenderTarget* target)
{
	for (auto& index : this->buttons)
	{
		index.second->render(target);
	}
}

void GameWinningState::render(sf::RenderTarget* target)
{
	if (!target)
	{
		target = this->window;
	}
	target->draw(this->background);
	this->renderButtons(target);
}