#pragma once
#include "State.h"
#include "Button.h"
class GameOverState :public State
{
	GameOverState(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~GameOverState();
	void endState();
	void updateButtons();
	void update();
	void renderButtons(sf::RenderTarget* target = nullptr);
	void render(sf::RenderTarget* target = nullptr);

private:
	sf::Texture background_texture;
	sf::RectangleShape background;
	sf::Font font;
	std::map<std::string, Button*>buttons;
	void initImage();
	void initFont();
	void initButton();
};

