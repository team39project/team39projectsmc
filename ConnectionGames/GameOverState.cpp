#include "GameOverState.h"
#include "GameOverState.h"
GameOverState::GameOverState(sf::RenderWindow* window, std::stack<State*>* states)
	: State(window, states)
{
	this->initImage();
	this->initFont();
	this->initButton();
}
GameOverState::~GameOverState()
{
	for (auto index = buttons.begin(); index != buttons.end(); ++index)
	{
		delete index->second;
	}
}
void GameOverState::endState()
{
}

void GameOverState::updateButtons()
{
	for (auto& index : this->buttons)
	{
		index.second->update(this->mousePosView);
	}

	if (this->buttons["Play_Again"]->getButtonPressed())
	{
		this->end = true;
	}
}
void GameOverState::update()
{
	this->updatemousePosition();
	this->updateButtons();

}
void GameOverState::renderButtons(sf::RenderTarget* target)
{

	for (auto& index : this->buttons)
	{
		index.second->render(target);
	}
}
void GameOverState::render(sf::RenderTarget* target)
{
	if (!target)
	{
		target = this->window;
	}
	target->draw(this->background);
	this->renderButtons(target);

}
void GameOverState::initImage()
{
	this->background.setSize(sf::Vector2f(static_cast<float>(this->window->getSize().x), static_cast<float>(this->window->getSize().y)));

	if (!this->background_texture.loadFromFile("Background/over.png"))
	{
		throw("Could not load image from file");
	}

	this->background.setTexture(&this->background_texture);
}
void GameOverState::initFont()
{
	if (!this->font.loadFromFile("arial.ttf"))
	{
		throw("Could not load font from file");
	}
}
void GameOverState::initButton()
{
	this->buttons["Play_Again"] = new Button(200, 300, 150, 50, &this->font, "Play again", sf::Color::Red, sf::Color::Cyan, sf::Color::Green);
}
